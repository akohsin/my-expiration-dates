public class Main {
    public static void main(String[] args) {

        Magazin magazin = new Magazin();
        magazin.addProductBatch("mlekovita", 500, -10, 40, PRODUCT_TYPE.MASLO);
        magazin.addProductBatch("3", 500, 9, 40, PRODUCT_TYPE.MLEKO);
        magazin.addProductBatch("2", 500, 10, 40, PRODUCT_TYPE.MLEKO);
        magazin.addProductBatch("3", 500, -95, 20, PRODUCT_TYPE.MLEKO);
        magazin.addProductBatch("4", 500, 120, 40, PRODUCT_TYPE.MLEKO);
        magazin.addProductBatch("5", 500, 9, 10, PRODUCT_TYPE.MLEKO);
        magazin.addProductBatch("6", 500, 10, 40, PRODUCT_TYPE.MLEKO);
        magazin.addProductBatch("7", 500, 9, 40, PRODUCT_TYPE.MLEKO);

        magazin.addProductBatch("prawdziwe", 600, 9, 40, PRODUCT_TYPE.MASLO);
        magazin.addProductBatch("8", 500, 10, 40, PRODUCT_TYPE.MLEKO);
        magazin.checkProduct(PRODUCT_TYPE.MLEKO);
        magazin.checkAndRemoveExpired();
        System.out.println();
        magazin.checkProduct(PRODUCT_TYPE.MLEKO);


    }
}
