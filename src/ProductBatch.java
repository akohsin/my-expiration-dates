import java.time.LocalDate;
import java.util.List;

public class ProductBatch {
    Product product;
    private int amount;
    PRODUCT_TYPE type;

    public ProductBatch(Product product, int amount, PRODUCT_TYPE type) {
        this.product = product;
        this.amount = amount;
        this.type = type;
    }

    public Product getProduct() {
        return product;
    }

    public int getAmount() {
        return amount;
    }

    public LocalDate getExpirationDate() {
        return product.getExpirationDate();
    }

    public PRODUCT_TYPE getType() {
        return type;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }


    public void print() {
        System.out.printf("%s : [ %s, %f, %d ] \n", product.getName(), type, (double) product.getPrice()/100.0, amount);
    }
}
