import java.time.LocalDate;


public class Product {
    private int price;
    private String name;
    private LocalDate expirationDate;

    public Product(String name, int price, int expirationTime) {
        this.name = name;
        this.price = price;
        this.expirationDate = LocalDate.now().plusDays(expirationTime);
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }
}
