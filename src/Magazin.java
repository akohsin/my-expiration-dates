import java.time.LocalDate;
import java.util.*;

public class Magazin {

    private SortedMap<LocalDate, List<ProductBatch>> magazin = new TreeMap<>(
            new Comparator<LocalDate>() {
                @Override
                public int compare(LocalDate o1, LocalDate o2) {
                    if (o1.isBefore(o2)) return 1;
                    else if (o1.equals(o2)) return 0;
                    else return 0;
                }
            });
    private Map<LocalDate, List<ProductBatch>> x;

    public void addProductBatch(String name, int price, int expirationTime, int amount, PRODUCT_TYPE typeOfProduct) {
        ProductBatch productBatch = new ProductBatch(new Product(name, price, expirationTime), amount, typeOfProduct);
        if (magazin.containsKey(productBatch.getExpirationDate())) {
            magazin.get(productBatch.getExpirationDate()).add(productBatch);
        } else {
            ArrayList<ProductBatch> tmp3 = new ArrayList<>();
            tmp3.add(productBatch);
            magazin.put(productBatch.getExpirationDate(), tmp3);
        }
    }

    public Product getProduct(PRODUCT_TYPE p) {
        for (Iterator<Map.Entry<LocalDate, List<ProductBatch>>> i = magazin.entrySet().iterator(); i.hasNext(); ) {
            for (Iterator<ProductBatch> ip = i.next().getValue().iterator(); i.hasNext(); ) {
                if (ip.next().type.equals(p)) {
                    if (ip.next().getAmount() > 1) {
                        ip.next().setAmount(ip.next().getAmount() - 1);
                        return ip.next().getProduct();
                    }
                    Product tmp = ip.next().getProduct();
                    ip.remove();
                    return tmp;
                }
            }
        }
        throw new NoProductException();
    }

    public void removeAllProduct(PRODUCT_TYPE p) {
        for (Iterator<Map.Entry<LocalDate, List<ProductBatch>>> i = magazin.entrySet().iterator(); i.hasNext(); ) {
            for (Iterator<ProductBatch> ip = i.next().getValue().iterator(); i.hasNext(); ) {
                if (ip.next().type.equals(p)) {
                    ip.remove();
                }
            }
        }
        throw new NoProductException();
    }

    public void checkProduct(PRODUCT_TYPE p) {
        for (Iterator<Map.Entry<LocalDate, List<ProductBatch>>> i = magazin.entrySet().iterator(); i.hasNext(); ) {
            List<ProductBatch> tmpList = i.next().getValue();
            for (Iterator<ProductBatch> ip = tmpList.iterator(); ip.hasNext(); ) {
                ProductBatch tmp = ip.next();
                if (tmp.type.equals(p)) {
                    tmp.print();
                }
            }
        }
//        throw new NoProductException();
    }

    public void checkAndRemoveExpired() {
        for (Iterator<Map.Entry<LocalDate, List<ProductBatch>>> i = magazin.entrySet().iterator(); i.hasNext(); ) {
//            Map.Entry<LocalDate, List<ProductBatch>> tmp = i.next();
//            if (tmp.getKey().isBefore(LocalDate.now())) {
//                for (Iterator<ProductBatch> ip = tmp.getValue().iterator(); i.hasNext(); ) {
//                    ip.next().print();
//                i.remove();
//            }
        if (i.next().getKey().isBefore(LocalDate.now()))i.remove();
        }
    }
}
